<?php
function GetPrice($from, $to) {
  $from = (trim(strtoupper($from)));
  $to = (trim(strtoupper($to)));
  $url = 'curl -s -H "CB-VERSION: 2017-12-06" "https://api.coinbase.com/v2/prices/'.$from.'-'.$to.'/spot"';
  $tmp = shell_exec($url);
  $data = json_decode($tmp, true);
  if ($data && $data['data'] && $data['data']['amount']) {
    return (float)$data['data']['amount'];
  }
  return null;
}
function assetsValueInDolar($user){
	$cashes=            $user->cashes()->where('spent', false)->get();
    $debts=             $user->debts()->where('paid', false)->get()         ;
    $deposits=          $user->deposits()->whereDate('end', '>', \Carbon\Carbon::now())->get()       ;
    $drawings=          $user->drawings()->where('using', true)->get()       ;
    $commodities=       $user->commodities()->where('sold', false)->get()    ;
    $bonds=             $user->bonds()->whereDate('end', '>',  \Carbon\Carbon::now())->get()     ;
    $repos=             $user->repos()->whereDate('end', '>',  \Carbon\Carbon::now())->get()          ;
    $stocks=            $user->stocks()->where('sold', false)->get()         ;
    $cryptocurrencies=  $user->cryptocurrencies()->where('spent', false)->get();
    $houses=            $user->houses()->where('sold', false)->get()        ;
    $cars=              $user->cars()->where('sold', false)->get()          ;
    $lands=             $user->lands()->where('sold', false)->get()          ;
    $animals=           $user->animals()->where('sold', false)->get()        ;
    $others=            $user->others()->where('sold', false)->get()        ;
    $totalCash=0;
    $totalDebt=0;
    $totalDeposit=0;
    $totalDrawing= 0;      
    $totalCommodity=    0;
    $totalBond=          0;
    $totalRepo=          0;
    $totalStock=         0;
    $totalCryptocurrency=0;
    $totalHouse=         0;
    $totalCar=           0;
    $totalLand=          0;
    $totalAnimal=        0;
    $totalOther=         0;
    if(!$cashes->isEmpty()){
        foreach($cashes as $cash){
            if($cash->currency != "usd"){
                $totalCash = $totalCash + str_replace(",","",trim(currency($cash->amount, $cash->currency, 'USD'),"$"));
            }else{
                $totalCash = $totalCash +$cash->amount;
            }
        }
    }
    if(!$debts->isEmpty()){
        foreach($debts as $debt){
            if($debt->currency != "usd"){
                $totalDebt = $totalDebt + str_replace(",","",trim(currency($debt->amount, $debt->currency, 'USD'),"$"));
            }else{
                $totalDebt = $totalDebt +$debt->amount;
            }
        }
    }
    if(!$deposits->isEmpty()){
        foreach($deposits as $deposit){
            if($deposit->currency != "usd"){
                $totalDeposit = $totalDeposit + str_replace(",","",trim(currency($deposit->amount, $deposit->currency, 'USD'),"$"));
            }else{
                $totalDeposit = $totalDeposit + $deposit->amount;
            }
        }
    }
    if(!$drawings->isEmpty()){
        foreach($drawings as $drawing){
            if($drawing->currency != "usd"){
                $totalDrawing = $totalDrawing + str_replace(",","",trim(currency($drawing->amount, $drawing->currency, 'USD'),"$"));
            }else{
                $totalDrawing = $totalDrawing +$drawing->amount;
            }
        }
    }
    if(!$commodities->isEmpty()){
        foreach($commodities as $commodity){
            if($commodity->currency != "usd"){
                $totalCommodity = $totalCommodity + str_replace(",","",trim(currency($commodity->amount, $commodity->currency, 'USD'),"$"));
            }else{
                $totalCommodity = $totalCommodity +$commodity->amount;
            }
        }
    }
    if(!$bonds->isEmpty()){
        foreach($bonds as $bond){
            if($bond->currency != "usd"){
                $totalBond = $totalBond + str_replace(",","",trim(currency($bond->amount, $bond->currency, 'USD'),"$"));
            }else{
                $totalBond = $totalBond +$bond->amount;
            }
        }
    }
    if(!$repos->isEmpty()){
        foreach($repos as $repo){
            if($repo->currency != "usd"){
                $totalRepo = $totalRepo + str_replace(",","",trim(currency($repo->amount, $repo->currency, 'USD'),"$"));
            }else{
                $totalRepo = $totalRepo +$repo->amount;
            }
        }
    }
    if(!$stocks->isEmpty()){
        foreach($stocks as $stock){
            if($repo->currency != "usd"){
                $totalStock = $totalStock + ($stock->amount*str_replace(",","",trim(currency($stock->price, $stock->currency, 'USD'),"$")));
            }else{
                $totalStock = $totalStock + ($stock->amount*$stock->price);
            }
        }
    }
    if(!$cryptocurrencies->isEmpty()){
        foreach($cryptocurrencies as $cryptocurrency){
            switch ($cryptocurrency->currency) {
            case "btc":
                $totalCryptocurrency = $totalCryptocurrency + ($cryptocurrency->amount*GetPrice("BTC", "USD") );
                break;
            case "xrp":
                $totalCryptocurrency = $totalCryptocurrency + ($cryptocurrency->amount*GetPrice("XRP", "USD") );
                break;
            case "eth":
                $totalCryptocurrency = $totalCryptocurrency + ($cryptocurrency->amount*GetPrice("XRP", "USD") );
                break;
            }
        }
    }
    if(!$houses->isEmpty()){
        foreach($houses as $house){
            if($house->currency != "usd"){
                $totalHouse = $totalHouse + str_replace(",","",trim(currency($house->price, $house->currency, 'USD'),"$"));
            }else{
                $totalHouse = $totalHouse + $house->price;
            }
        }
    }
    if(!$cars->isEmpty()){
        foreach($cars as $car){
            if($car->currency != "usd"){
                $totalCar = $totalCar + str_replace(",","",trim(currency($car->price, $car->currency, 'USD'),"$"));
            }else{
                $totalCar = $totalCar + $car->price;
            }
        }
    }
    if(!$lands->isEmpty()){
        foreach($lands as $land){
            if($land->currency != "usd"){
                $totalLand = $totalLand + str_replace(",","",trim(currency($land->price, $land->currency, 'USD'),"$"));
            }else{
                $totalLand = $totalLand + $land->price;
            }
        }
    }
    if(!$animals->isEmpty()){
        foreach($animals as $animal){
            if($animal->currency != "usd"){
                $totalAnimal = $totalAnimal + ($animal->amount*str_replace(",","",trim(currency($animal->price, $animal->currency, 'USD'),"$")));
            }else{
                $totalAnimal = $totalAnimal + ($animal->amount*$animal->price);
            }
        }
    }
    if(!$others->isEmpty()){
        foreach($others as $other){
            if($other->currency != "usd"){
                $totalOther = $totalOther + ($other->amount*str_replace(",","",trim(currency($other->price, $other->currency, 'USD'),"$")));
            }else{
                $totalOther = $totalOther + ($other->amount*$other->price);
            }
        }
    }
    $total=array('totalCash'=>$totalCash,
    		'totalDebt'=>$totalDebt,
    		'totalDeposit'		=>$totalDeposit	,			
    		'totalDrawing'   		=> $totalDrawing,
			'totalCommodity'    	=>$totalCommodity,
			'totalBond'       	=>$totalBond     ,
			'totalRepo'       	=>$totalRepo         ,
			'totalStock'      	=>$totalStock        ,
			'totalCryptocurrency'	=>$totalCryptocurrency,
			'totalHouse'      	=>$totalHouse        ,
			'totalCar'         	=>$totalCar         ,
			'totalLand'         	=>$totalLand         ,
			'totalAnimal'       	=>$totalAnimal       ,
			'totalOther'        	=>$totalOther        
		);
    return $total;
}

function userHasNoAsset($user){
    $cashes=            $user->cashes()->where('spent', false)->get();
    $debts=             $user->debts()->where('paid', false)->get()         ;
    $deposits=          $user->deposits()->whereDate('end', '>', \Carbon\Carbon::now())->get()       ;
    $drawings=          $user->drawings()->where('using', true)->get()       ;
    $commodities=       $user->commodities()->where('sold', false)->get()    ;
    $bonds=             $user->bonds()->whereDate('end', '>',  \Carbon\Carbon::now())->get()     ;
    $repos=             $user->repos()->whereDate('end', '>',  \Carbon\Carbon::now())->get()          ;
    $stocks=            $user->stocks()->where('sold', false)->get()         ;
    $cryptocurrencies=  $user->cryptocurrencies()->where('spent', false)->get();
    $houses=            $user->houses()->where('sold', false)->get()        ;
    $cars=              $user->cars()->where('sold', false)->get()          ;
    $lands=             $user->lands()->where('sold', false)->get()          ;
    $animals=           $user->animals()->where('sold', false)->get()        ;
    $others=            $user->others()->where('sold', false)->get()        ;
	if($cashes->isEmpty() && $debts->isEmpty() && $deposits->isEmpty() && $drawings->isEmpty() && $commodities->isEmpty() && $bonds->isEmpty() && $repos->isEmpty() && $stocks->isEmpty() && $cryptocurrencies->isEmpty() && $houses->isEmpty() && $cars->isEmpty() && $lands->isEmpty() && $animals->isEmpty() && $others->isEmpty()){
		return true;
	}else{
		return false;
	}
}
