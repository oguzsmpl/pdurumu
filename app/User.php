<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function cashes(){

       return $this->hasMany('App\Cash');
    }
    public function debts(){

       return $this->hasMany('App\Debt');
    }
    public function deposits(){

       return $this->hasMany('App\Deposit');
    }
    public function drawings(){

       return $this->hasMany('App\Drawing');
    }
    public function commodities(){

       return $this->hasMany('App\Commodity');
    }
    public function bonds(){

       return $this->hasMany('App\Bond');
    }
    public function repos(){

       return $this->hasMany('App\Repo');
    }
    public function stocks(){

       return $this->hasMany('App\Stock');
    }
    public function cryptocurrencies(){

       return $this->hasMany('App\Cryptocurrency');
    }
    public function houses(){

       return $this->hasMany('App\House');
    }
    public function cars(){

       return $this->hasMany('App\Car');
    }
    public function lands(){

       return $this->hasMany('App\Land');
    }
    public function animals(){

       return $this->hasMany('App\Animal');
    }
    public function others(){

       return $this->hasMany('App\Other');
    }
    public function wealths(){

       return $this->hasMany('App\Wealth');
    }
}
