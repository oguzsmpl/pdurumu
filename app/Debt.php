<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Debt extends Model
{
    protected $guarded = [];
    public function user(){
        $this->belongsTo('App\User');
    }
}
