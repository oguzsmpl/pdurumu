<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAnimal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'type'=>'required|max:30',
                'amount'=>'required|numeric|between:0,2147000000',
                'price'=>'required|numeric|between:0,99999999',
                'currency'=>'required|In:try,usd,eur,gbp,rub,jpy,cny,inr,chf,aud,cad',
                'possesion_date'=>'required|date',
            
        ];
    }
}
