<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Car;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $cars = $user->cars()->orderBy('updated_at','desc')->paginate(10);
        return view('index_pages.car',compact('cars','user'));
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('create_pages.car',  compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $possesion_date = $request->possesion_date;
        $possesion_date = date("Y-m-d", strtotime($possesion_date));
        $user->cars()->create(['currency'=>$request->currency,'model'=>$request->model,'price'=>$request->price,'possesion_date'=>$possesion_date,'commission_rate'=>$request->commission_rate]);
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('CarController@index');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($car_id)
    {
        $user = Auth::user();
        $car = car::findOrFail($car_id);
        return view('edit_pages.car',  compact('user','car'));
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $car_id)
    {
        $user = Auth::user();
        $car= $user->cars()->findOrFail($car_id);
        $possesion_date = $request->possesion_date;
        $possesion_date = date("Y-m-d", strtotime($possesion_date));
        $sold_date = $request->sold_date;
        $sold_date == null ? $sold_date = null : $sold_date = date("Y-m-d", strtotime($sold_date));
        $car->update(['currency'=>$request->currency,'model'=>$request->model,'price'=>$request->price,'commission_rate'=>$request->commission_rate,'possesion_date'=>$possesion_date,'sold'=>$request->sold,'sold_date'=>$sold_date,'price_sale'=>$request->price_sale]);
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('CarController@index',['user_id'=>$user->id]);
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($car_id)
    {
        $user = Auth::user();
        $car= Car::findOrFail($car_id);
        $car->delete();
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('CarController@index');
    
    }
}
