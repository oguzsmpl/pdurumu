<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\House;

class HouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $houses = $user->houses()->orderBy('updated_at','desc')->paginate(10);
        return view('index_pages.house',compact('houses','user'));
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('create_pages.house',  compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $possesion_date = $request->possesion_date;
        $possesion_date = date("Y-m-d", strtotime($possesion_date));
        $user->houses()->create(['currency'=>$request->currency,'location'=>$request->location,'area'=>$request->area,'price'=>$request->price,'commission_rate'=>$request->commission_rate,'possesion_date'=>$possesion_date]);
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('HouseController@index');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($house_id)
    {
        $user = Auth::user();
        $house = House::findOrFail($house_id);
        return view('edit_pages.house',  compact('user','house'));
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $house_id)
    {
        $user = Auth::user();
        $drawing= $user->houses()->findOrFail($house_id);
        $possesion_date = $request->possesion_date;
        $possesion_date = date("Y-m-d", strtotime($possesion_date));
        $sold_date = $request->sold_date;
        $sold_date == null ? $sold_date = null : $sold_date = date("Y-m-d", strtotime($sold_date));
        $house->update(['currency'=>$request->currency,'location'=>$request->location,'area'=>$request->area,'price'=>$request->price,'commission_rate'=>$request->commission_rate,'possesion_date'=>$possesion_date,'sold'=>$request->sold,'sold_date'=>$sold_date,'price_sale'=>$request->price_sale]);
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('HouseController@index',['user_id'=>$user->id]);
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($house_id)
    {
        $user = Auth::user();
        $house= House::findOrFail($house_id);
        $house->delete();
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('HouseController@index');
    
    }
}
