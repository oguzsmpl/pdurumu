<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Commodity;

class CommodityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $commodities = $user->commodities()->orderBy('updated_at','desc')->paginate(10);
        return view('index_pages.commodity',compact('commodities','user'));
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('create_pages.commodity',  compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $possesion_date = $request->possesion_date;
        $possesion_date = date("Y-m-d", strtotime($possesion_date));
        $user->commodities()->create(['currency'=>$request->currency,'type'=>$request->type,'amount'=>$request->amount,'price'=>$request->price,'commission_rate'=>$request->commission_rate,'possesion_date'=>$possesion_date]);
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('CommodityController@index');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($commodity_id)
    {
        $user = Auth::user();
        $commodity = Commodity::findOrFail($commodity_id);
        return view('edit_pages.commodity',  compact('user','commodity'));
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $commodity_id)
    {
        $user = Auth::user();
        $commodity= $user->commodities()->findOrFail($commodity_id);
        $possesion_date = $request->possesion_date;
        $possesion_date = date("Y-m-d", strtotime($possesion_date));
        $sold_date = $request->sold_date;
        $sold_date == null ? $sold_date = null : $sold_date = date("Y-m-d", strtotime($sold_date));
        $commodity->update(['currency'=>$request->currency,'type'=>$request->type,'amount'=>$request->amount,'price'=>$request->price,'commission_rate'=>$request->commission_rate,'possesion_date'=>$possesion_date,'sold'=>$request->sold,'sold_date'=>$sold_date,'price_sale'=>$request->price_sale]);
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('CommodityController@index',['user_id'=>$user->id]);
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($commodity_id)
    {
        $user = Auth::user();
        $commodity= Commodity::findOrFail($commodity_id);
        $commodity->delete();
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('CommodityController@index');
    
    }
}
