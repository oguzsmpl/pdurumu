<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Cash;

class CashController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $cashes = $user->cashes()->orderBy('updated_at','desc')->paginate(10);
        return view('index_pages.cash',compact('cashes','user'));
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('create_pages.cash',  compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $possesion_date = $request->possesion_date;
        $possesion_date = date("Y-m-d", strtotime($possesion_date));
        $user->cashes()->create(['currency'=>$request->currency,'amount'=>$request->amount,'possesion_date'=>$possesion_date]);
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('CashController@index');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($cash_id)
    {
        $user = Auth::user();
        $cash = Cash::findOrFail($cash_id);
        return view('edit_pages.cash',  compact('user','cash'));
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $cash_id)
    {
        $user = Auth::user();
        $cash= $user->cashes()->findOrFail($cash_id);
        $possesion_date = $request->possesion_date;
        $possesion_date = date("Y-m-d", strtotime($possesion_date));
        $spending_date = $request->spending_date;
        $spending_date == null ? $spending_date = null : $spending_date = date("Y-m-d", strtotime($spending_date));
        $cash->update(['currency'=>$request->currency,'amount'=>$request->amount,'possesion_date'=>$possesion_date,'why_spent'=>$request->why_spent,'spent'=>$request->spent,'spending_date'=>$spending_date]);
        return redirect()->action('CashController@index',['user_id'=>$user->id]);
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($cash_id)
    {
        $user = Auth::user();
        $cash= Cash::findOrFail($cash_id);
        $cash->delete();
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('CashController@index');
    
    }
}
