<?php

namespace App\Http\Controllers;

use App\Wealth;
use Illuminate\Http\Request;
use Auth;
use Khill\Lavacharts\Lavacharts;

class WealthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if(userHasNoAsset($user)== true){
            $userHasNoAsset = true;
        }else{
            $lavaWealth = new Lavacharts;
            $wealths = $user->wealths()->orderBy('id', 'desc')->limit(12)->get();
            $wealthNet  = $lavaWealth->DataTable();
            $wealthNet->addDateColumn(__('portfolio.dates'))
                ->addNumberColumn(__('portfolio.wealth_net'));
            foreach ($wealths as $wealth){
                $wealthNet->addRow([$wealth->created_at->format('Y-m-d'), $wealth->amount]);
            }
            $lavaWealth->LineChart('Wealth', $wealthNet,['title' => 'Wealth']);
            $userHasNoAsset = false;
        }
        return view('index_pages.wealth',compact('userHasNoAsset','lavaWealth'));
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($user)
    {
        $amount = $this->calculateValue($user);
        $user->wealths()->create(['amount'=>$amount]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Value  $value
     * @return \Illuminate\Http\Response
     */
    public function show(Value $value)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Value  $value
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Value $value)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Value  $value
     * @return \Illuminate\Http\Response
     */
    public function destroy(Value $value)
    {
        //
    }
    public function calculateValue($user){
        $total = assetsValueInDolar($user);
        $valueOfUser = array_sum($total) - 2*$total['totalDebt'];
        return $valueOfUser;    
   }
}
