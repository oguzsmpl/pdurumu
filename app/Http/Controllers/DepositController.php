<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Deposit;

class DepositController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $deposits = $user->deposits()->orderBy('updated_at','desc')->paginate(10);
        return view('index_pages.deposit',compact('deposits','user'));
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('create_pages.deposit',  compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $start = $request->start;
        $start = date("Y-m-d", strtotime($start));
        $end = $request->end;
        $end = date("Y-m-d", strtotime($end));
        $user->deposits()->create(['currency'=>$request->currency,'amount'=>$request->amount,'name_of_bank'=>$request->name_of_bank,'interest_rate'=>$request->interest_rate,'withholding_rate'=>$request->withholding_rate,'start'=>$start,'end'=>$end]);
        return redirect()->action('DepositController@index');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($deposit_id)
    {
        $user = Auth::user();
        $deposit = deposit::findOrFail($deposit_id);
        return view('edit_pages.deposit',  compact('user','deposit'));
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $deposit_id)
    {
        $user = Auth::user();
        $deposit= $user->deposits()->findOrFail($deposit_id);
        $start = $request->start;
        $start = date("Y-m-d", strtotime($start));
        $end = $request->end;
        $end == null ? $end = null : $end = date("Y-m-d", strtotime($end));
        $deposit->update(['currency'=>$request->currency,'amount'=>$request->amount,'name_of_bank'=>$request->name_of_bank,'interest_rate'=>$request->interest_rate,'withholding_rate'=>$request->withholding_rate,'start'=>$start,'end'=>$end]);
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('DepositController@index',['user_id'=>$user->id]);
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($deposit_id)
    {
        $user = Auth::user();
        $deposit= Deposit::findOrFail($deposit_id);
        $deposit->delete();
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('DepositController@index');
    
    }
}
