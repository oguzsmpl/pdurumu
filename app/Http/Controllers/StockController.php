<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Stock;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $stocks = $user->stocks()->orderBy('updated_at','desc')->paginate(10);
        return view('index_pages.stock',compact('stocks','user'));
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('create_pages.stock',  compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $possesion_date = $request->possesion_date;
        $possesion_date = date("Y-m-d", strtotime($possesion_date));
        $user->stocks()->create(['currency'=>$request->currency,'name'=>$request->name,'amount'=>$request->amount,'price'=>$request->price,'commission_rate'=>$request->commission_rate,'possesion_date'=>$possesion_date]);
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('StockController@index');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($stock_id)
    {
        $user = Auth::user();
        $stock = Stock::findOrFail($stock_id);
        return view('edit_pages.stock',  compact('user','stock'));
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $stock_id)
    {
        $user = Auth::user();
        $stock= $user->stocks()->findOrFail($stock_id);
        $possesion_date = $request->possesion_date;
        $possesion_date = date("Y-m-d", strtotime($possesion_date));
        $sold_date = $request->sold_date;
        $sold_date == null ? $sold_date = null : $sold_date = date("Y-m-d", strtotime($sold_date));
        $stock->update(['currency'=>$request->currency,'name'=>$request->name,'amount'=>$request->amount,'price'=>$request->price,'commission_rate'=>$request->commission_rate,'possesion_date'=>$possesion_date,'sold'=>$request->sold,'sold_date'=>$sold_date,'price_current'=>$request->price_current]);
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('StockController@index',['user_id'=>$user->id]);
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($stock_id)
    {
        $user = Auth::user();
        $stock= Stock::findOrFail($stock_id);
        $stock->delete();
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('StockController@index');
    
    }
}
