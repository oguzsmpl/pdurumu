<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Cryptocurrency;

class CryptocurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $cryptocurrencies = $user->cryptocurrencies()->orderBy('updated_at','desc')->paginate(10);
        return view('index_pages.cryptocurrency',compact('cryptocurrencies','user'));
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('create_pages.cryptocurrency',  compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $possesion_date = $request->possesion_date;
        $possesion_date = date("Y-m-d", strtotime($possesion_date));
        $user->cryptocurrencies()->create(['currency'=>$request->currency,'amount'=>$request->amount,'possesion_date'=>$possesion_date]);
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('CryptocurrencyController@index');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($cryptocurrency_id)
    {
        $user = Auth::user();
        $cryptocurrency = cryptocurrency::findOrFail($cryptocurrency_id);
        return view('edit_pages.cryptocurrency',  compact('user','cryptocurrency'));
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $cryptocurrency_id)
    {
        $user = Auth::user();
        $cryptocurrency= $user->cryptocurrencies()->findOrFail($cryptocurrency_id);
        $possesion_date = $request->possesion_date;
        $possesion_date = date("Y-m-d", strtotime($possesion_date));
        $spending_date = $request->spending_date;
        $spending_date == null ? $spending_date = null : $spending_date = date("Y-m-d", strtotime($spending_date));
        $cryptocurrency->update(['currency'=>$request->currency,'amount'=>$request->amount,'possesion_date'=>$possesion_date,'why_spent'=>$request->why_spent,'spent'=>$request->spent,'spending_date'=>$spending_date]);
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('CryptocurrencyController@index',['user_id'=>$user->id]);
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($cryptocurrency_id)
    {
        $user = Auth::user();
        $cryptocurrency= Cryptocurrency::findOrFail($cryptocurrency_id);
        $cryptocurrency->delete();
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('CryptocurrencyController@index');
    
    }
}
