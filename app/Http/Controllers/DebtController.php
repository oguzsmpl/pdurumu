<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use App\Debt;

class DebtController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $debts = $user->debts()->orderBy('updated_at','desc')->paginate(10);
        return view('index_pages.debt',compact('debts','user'));
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('create_pages.debt',  compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $possesion_date = $request->possesion_date;
        $possesion_date = date("Y-m-d", strtotime($possesion_date));
        $user->debts()->create(['currency'=>$request->currency,'who'=>$request->who,'amount'=>$request->amount,'possesion_date'=>$possesion_date]);
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('DebtController@index');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($debt_id)
    {
        $user = Auth::user();
        $debt = Debt::findOrFail($debt_id);
        return view('edit_pages.debt',  compact('user','debt'));
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $debt_id)
    {
        $user = Auth::user();
        $debt= $user->debts()->findOrFail($debt_id);
        $possesion_date = $request->possesion_date;
        $possesion_date = date("Y-m-d", strtotime($possesion_date));
        $paid_date = $request->paid_date;
        $paid_date == null ? $paid_date = null : $paid_date = date("Y-m-d", strtotime($possesion_date));
        $debt->update(['currency'=>$request->currency,'amount'=>$request->amount,'why_taken'=>$request->why_taken,'possesion_date'=>$possesion_date,'paid_date'=>$paid_date,'paid'=>$request->paid]);
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('DebtController@index',['user_id'=>$user->id]);
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($debt_id)
    {
        $user = Auth::user();
        $debt= Debt::findOrFail($debt_id);
        $debt->delete();
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('DebtController@index');
    
    }
}
