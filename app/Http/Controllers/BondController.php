<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Bond;

class BondController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $bonds = $user->bonds()->orderBy('updated_at','desc')->paginate(10);
        return view('index_pages.bond',compact('bonds','user'));
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('create_pages.bond',  compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $start = $request->start;
        $start = date("Y-m-d", strtotime($start));
        $end = $request->end;
        $end = date("Y-m-d", strtotime($end));
        $user->bonds()->create(['currency'=>$request->currency,'amount'=>$request->amount,'name_of_bank'=>$request->name_of_bank,'interest_rate'=>$request->interest_rate,'withholding_rate'=>$request->withholding_rate,'start'=>$start,'end'=>$end]);
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('BondController@index');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($bond_id)
    {
        $user = Auth::user();
        $bond = bond::findOrFail($bond_id);
        return view('edit_pages.bond',  compact('user','bond'));
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $bond_id)
    {
        $user = Auth::user();
        $bond= $user->bonds()->findOrFail($bond_id);
        $start = $request->start;
        $start = date("Y-m-d", strtotime($start));
        $end = $request->end;
        $end == null ? $end = null : $end = date("Y-m-d", strtotime($end));
        $bond->update(['currency'=>$request->currency,'amount'=>$request->amount,'name_of_bank'=>$request->name_of_bank,'interest_rate'=>$request->interest_rate,'withholding_rate'=>$request->withholding_rate,'start'=>$start,'end'=>$end]);
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('BondController@index',['user_id'=>$user->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($bond_id)
    {
        $user = Auth::user();
        $bond= Bond::findOrFail($bond_id);
        $bond->delete();
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('BondController@index');
    
    }
}
