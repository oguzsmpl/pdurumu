<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Animal;
use App\Http\Requests\StoreAnimal;

class AnimalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $animals = $user->animals()->orderBy('updated_at','desc')->paginate(10);
        return view('index_pages.animal',compact('animals','user'));
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('create_pages.animal',  compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAnimal $request)
    {
        $request->validate([
    'possesion_date' => 'required'
]);
        $user = Auth::user();
        $possesion_date = $request->possesion_date;
        $possesion_date = date("Y-m-d", strtotime($possesion_date));
        $user->animals()->create(['type'=>$request->type,'amount'=>$request->amount,'price'=>$request->price,'commission_rate'=>$request->commission_rate,'possesion_date'=>$possesion_date]);
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('AnimalController@index');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($animal_id)
    {
        $user = Auth::user();
        $animal = animal::findOrFail($animal_id);
        return view('edit_pages.animal',  compact('user','animal'));
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $animal_id)
    {
        $user = Auth::user();
        $animal= $user->animals()->findOrFail($animal_id);
        $possesion_date = $request->possesion_date;
        $possesion_date = date("Y-m-d", strtotime($possesion_date));
        $sold_date = $request->sold_date;
        $sold_date == null ? $sold_date = null : $sold_date = date("Y-m-d", strtotime($sold_date));
        $animal->update(['type'=>$request->type,'amount'=>$request->amount,'price'=>$request->price,'commission_rate'=>$request->commission_rate,'possesion_date'=>$possesion_date,'sold'=>$request->sold,'sold_date'=>$sold_date,'price_sale'=>$request->price_sale]);
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('AnimalController@index',['user_id'=>$user->id]);
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($animal_id)
    {
        $user = Auth::user();
        $animal= animal::findOrFail($animal_id);
        $animal->delete();
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('AnimalController@index');
    
    }
}
