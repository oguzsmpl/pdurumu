<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Charts\PortfolioState;
use Auth;
use Khill\Lavacharts\Lavacharts;
use Config\Swap;
use App\Http\Controllers\WealthController;
use Carbon\Carbon;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $user = Auth::user();
        if(userHasNoAsset($user)== true){
            $userHasNoAsset = true;
        }else{
            $lavaPdurumu = new Lavacharts;
            $pdurumu = $lavaPdurumu->DataTable();
            $total = assetsValueInDolar($user);
            $pdurumu->addStringColumn(__('portfolio.portfolio_state'))
                    ->addNumberColumn('Percent')
                    ->addRow([__('portfolio.cash'), $total['totalCash']])
                    ->addRow([__('portfolio.debt'), $total['totalDebt']])
                    ->addRow([__('portfolio.other'), $total['totalOther']])
                    ->addRow([__('portfolio.deposit_account'), $total['totalDeposit']])
                    ->addRow([__('portfolio.drawing_account'), $total['totalDrawing']])
                    ->addRow([__('portfolio.commodity'), $total['totalCommodity']])
                    ->addRow([__('portfolio.bond'), $total['totalBond']  ])
                    ->addRow([__('portfolio.repo'), $total['totalRepo']     ])
                    ->addRow([__('portfolio.stock'), $total['totalStock']     ])
                    ->addRow([__('portfolio.cryptocurrency'), $total['totalCryptocurrency']])
                    ->addRow([__('portfolio.house'), $total['totalHouse']])
                    ->addRow([__('portfolio.car'), $total['totalCar']    ])
                    ->addRow([__('portfolio.land'), $total['totalLand']     ])
                    ->addRow([__('portfolio.animal'), $total['totalAnimal']  ]);
            $lavaPdurumu->PieChart('Portfolio', $pdurumu,[
                            'is3D'   => true]);
            $userHasNoAsset = false;
        }
        return view('home',compact('lavaPdurumu','userHasNoAsset','lavaWealth'));
    }
}

