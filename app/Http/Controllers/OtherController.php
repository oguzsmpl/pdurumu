<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Other;

class OtherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $others = $user->others()->orderBy('updated_at','desc')->paginate(10);
        return view('index_pages.other',compact('others','user'));
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('create_pages.other',  compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $possesion_date = $request->possesion_date;
        $possesion_date = date("Y-m-d", strtotime($possesion_date));
        $user->others()->create(['currency'=>$request->currency,'name_of_asset'=>$request->name_of_asset,'amount'=>$request->amount,'price'=>$request->price,'commission_rate'=>$request->commission_rate,'possesion_date'=>$possesion_date]);
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('OtherController@index');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($other_id)
    {
        $user = Auth::user();
        $other = Other::findOrFail($other_id);
        return view('edit_pages.other',  compact('user','other'));
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $other_id)
    {
        $user = Auth::user();
        $other= $user->others()->findOrFail($other_id);
        $possesion_date = $request->possesion_date;
        $possesion_date = date("Y-m-d", strtotime($possesion_date));
        $sold_date = $request->sold_date;
        $sold_date == null ? $sold_date = null : $sold_date = date("Y-m-d", strtotime($sold_date));
        $other->update(['currency'=>$request->currency,'name_of_asset'=>$request->name_of_asset,'amount'=>$request->amount,'price'=>$request->price,'commission_rate'=>$request->commission_rate,'possesion_date'=>$possesion_date,'sold'=>$request->sold,'sold_date'=>$sold_date]);
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('OtherController@index',['user_id'=>$user->id]);
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($other_id)
    {
        $user = Auth::user();
        $other= Other::findOrFail($other_id);
        $other->delete();
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('OtherController@index');
    
    }
}
