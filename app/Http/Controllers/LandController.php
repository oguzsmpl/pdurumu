<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Land;

class LandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $lands = $user->lands()->orderBy('updated_at','desc')->paginate(10);
        return view('index_pages.land',compact('lands','user'));
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('create_pages.land',  compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $possesion_date = $request->possesion_date;
        $possesion_date = date("Y-m-d", strtotime($possesion_date));
        $user->lands()->create(['currency'=>$request->currency,'location'=>$request->location,'area'=>$request->area,'price'=>$request->price,'commission_rate'=>$request->commission_rate,'possesion_date'=>$possesion_date]);
        return redirect()->action('LandController@index');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($land_id)
    {
        $user = Auth::user();
        $land = Land::findOrFail($land_id);
        return view('edit_pages.land',  compact('user','land'));
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $land_id)
    {
        $user = Auth::user();
        $land= $user->lands()->findOrFail($land_id);
        $possesion_date = $request->possesion_date;
        $possesion_date = date("Y-m-d", strtotime($possesion_date));
        $sold_date = $request->sold_date;
        $sold_date == null ? $sold_date = null : $sold_date = date("Y-m-d", strtotime($sold_date));
        $land->update(['currency'=>$request->currency,'location'=>$request->location,'area'=>$request->area,'price'=>$request->price,'commission_rate'=>$request->commission_rate,'possesion_date'=>$possesion_date,'sold'=>$request->sold,'sold_date'=>$sold_date,'price_sale'=>$request->price_sale]);
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('LandController@index',['user_id'=>$user->id]);
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($land_id)
    {
        $user = Auth::user();
        $land= Land::findOrFail($land_id);
        $land->delete();
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('LandController@index');
    
    }
}
