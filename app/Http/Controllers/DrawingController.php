<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Drawing;

class DrawingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $drawings = $user->drawings()->orderBy('updated_at','desc')->paginate(10);
        return view('index_pages.drawing',compact('drawings','user'));
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('create_pages.drawing',  compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $start = $request->start;
        $start = date("Y-m-d", strtotime($start));
        $end = $request->end;
        $end = date("Y-m-d", strtotime($end));
        $user->drawings()->create(['currency'=>$request->currency,'amount'=>$request->amount,'name_of_bank'=>$request->name_of_bank,'iban'=>$request->iban,'start'=>$start,'end'=>$end]);
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('DrawingController@index');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($drawing_id)
    {
        $user = Auth::user();
        $drawing = Drawing::findOrFail($drawing_id);
        return view('edit_pages.drawing',  compact('user','drawing'));
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $drawing_id)
    {
        $user = Auth::user();
        $drawing= $user->drawings()->findOrFail($drawing_id);
        $start = $request->start;
        $start = date("Y-m-d", strtotime($start));
        $end = $request->end;
        $end == null ? $end = null : $end = date("Y-m-d", strtotime($end));
        $drawing->update(['currency'=>$request->currency,'amount'=>$request->amount,'name_of_bank'=>$request->name_of_bank,'iban'=>$request->iban,'start'=>$start,'end'=>$end,'using'=>$request->using]);
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('DrawingController@index',['user_id'=>$user->id]);
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($drawing_id)
    {
        $user = Auth::user();
        $drawing= Drawing::findOrFail($drawing_id);
        $drawing->delete();
        \Session::flash('alert-success',__('message.success'));
        return redirect()->action('DrawingController@index');
    
    }
}
