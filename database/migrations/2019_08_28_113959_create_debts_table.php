<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDebtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('who', 20);
            $table->enum('currency', ['try','usd','eur','gbp','rub','jpy','cny','inr','chf','aud','cad']);
            $table->decimal('amount', 10, 2);
            $table->boolean('paid')->default(false);
            $table->enum('why_taken', ['Entertainment','Education','Shopping','Health','Kids','Food','Gift','Investment','Bill','Transport','Tax','Other'])->nullable();
            $table->date('possesion_date');
            $table->date('paid_date')->nullable();
            $table->unsignedInteger('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('debts');
    }
}
