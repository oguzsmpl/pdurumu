<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repos', function (Blueprint $table) {
            $table->bigIncrements('id');
             $table->string('name_of_bank', 20);
            $table->decimal('amount', 10, 2);
            $table->enum('currency', ['try','usd','eur','gbp','rub','jpy','cny','inr','chf','aud','cad']);
            $table->decimal('interest_rate', 5, 2);
            $table->decimal('withholding_rate', 5, 2);
            $table->date('start');
            $table->date('end');
            $table->unsignedInteger('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repos');
    }
}
