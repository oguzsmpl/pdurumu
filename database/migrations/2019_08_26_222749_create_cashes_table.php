<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cashes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('currency', ['try','usd','eur','gbp','rub','jpy','cny','inr','chf','aud','cad']);
            $table->decimal('amount', 10, 2);
            $table->date('possesion_date');
            $table->date('spending_date')->nullable();
            $table->boolean('spent')->default(false);
            $table->enum('why_spent', ['Entertainment','Education','Shopping','Health','Kids','Food','Gift','Investment','Bill','Transport','Tax','Other'])->nullable();
            $table->unsignedInteger('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cashes');
    }
}
