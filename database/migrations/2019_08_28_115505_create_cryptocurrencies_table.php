<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCryptocurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cryptocurrencies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->decimal('amount', 10, 5);
            $table->enum('currency', ['btc','xrp','eth','bch','ltc']);
            $table->date('possesion_date');
            $table->date('spending_date')->nullable();
            $table->boolean('spent')->default(false);
            $table->enum('why_spent', ['Entertainment','Education','Shopping','Health','Kids','Food','Gift','Investment','Bill','Transport','Tax','Other'])->nullable();
            $table->unsignedInteger('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cryptocurrencies');
    }
}
