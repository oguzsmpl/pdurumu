<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lands', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('location', 30);
            $table->decimal('area', 10, 2);
            $table->decimal('price', 10, 2);
            $table->enum('currency', ['try','usd','eur','gbp','rub','jpy','cny','inr','chf','aud','cad']);
            $table->decimal('price_sale', 10, 2)->nullable();
            $table->decimal('commission_rate', 6, 3)->nullable();
            $table->boolean('sold')->default(false);
            $table->date('possesion_date');
            $table->date('sold_date')->nullable();
            $table->unsignedInteger('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lands');
    }
}
