<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDrawingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drawings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_of_bank', 50)->nullable();
            $table->string('iban',26)->nullable();
            $table->decimal('amount', 10, 2)->nullable();
            $table->enum('currency', ['try','usd','eur','gbp','rub','jpy','cny','inr','chf','aud','cad']);
            $table->date('start');
            $table->date('end')->nullable();
            $table->boolean('using')->default(true);
            $table->unsignedInteger('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drawings');
    }
}
