@extends('layouts.app')
@section('content')
    <div class='container'>
        <div class='row'>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">@lang('portfolio.wealth_change_graph_in_dollar')</div>
                        <div class="card-body" id="wealthNet">
                            @if($userHasNoAsset == true)
                            <p style="color:red">@lang('message.no_data')</p>
                            @else
                            {!! $lavaWealth->render('LineChart','Wealth','wealthNet') !!}
                            @endif()
                        </div>
                    </div>   
                </div>
        </div>  
    </div>
@endsection