@extends('layouts.app')
@section('content')
    <div class='container'>
        <div class='row'>
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-primary">
                        <div class="panel-heading">@lang('portfolio.drawing_accounts')</div>
                        @if($drawings->count() >0)
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>@lang('portfolio.name_of_bank')</th>
                                                <th>@lang('portfolio.currency')</th>
                                                <th>@lang('portfolio.amount')</th>
                                                <th>@lang('portfolio.date_start')</th>
                                                <th>@lang('portfolio.using')</th>
                                                <th>@lang('portfolio.edit')</th>
                                                <th>@lang('portfolio.delete')</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($drawings as $drawing)
                                            <tr>
                                                <td>{{$drawing->name_of_bank}}</td>
                                                <td>{{$drawing->currency}}</td>
                                                <td>{{number_format($drawing->amount,2,'.','')}}</td>  
                                                <td>{{date("d.m.Y", strtotime($drawing->start))}}</td>
                                                <td>{{$drawing->using ? __('portfolio.yes') : __('portfolio.no')}}</td>
                                                <td>
                                                    <a class="btn btn-primary" href="{{route("drawings.edit",['id'=>$drawing->id]) }}" role="button">@lang('portfolio.edit')</a></div>
                                                </td>
                                                <td>
                                                    {{ Form::open(['action' => ['DrawingController@destroy', 'id'=>$drawing->id], 'method' => 'delete','onsubmit' => 'return ConfirmDelete()']) }}
                                                        {{ Form::submit(__('portfolio.delete'), ['class'=>'btn btn-danger btn-mini']) }}
                                                    {{ Form::close() }}
                                                </td>
                                            </tr>
                                            @endforeach 
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-10 col-md-offset-2">{{ $drawings->links() }}</div>
                            </div>
                        @else
                            <p align="center" style="color:red">@lang('message.no_data')</p> 
                        @endif
                    </div>   
                </div>
        </div>  
    </div>
@endsection