@extends('layouts.app')
@section('content')
    <div class='container'>
        <div class='row'>
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-primary">
                        <div class="panel-heading">@lang('portfolio.lands')</div>
                        @if($lands->count() >0)
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>@lang('portfolio.location')</th>
                                                <th>@lang('portfolio.area')</th>
                                                <th>@lang('portfolio.price')</th>
                                                <th>@lang('portfolio.commission_rate')</th>
                                                <th>@lang('portfolio.possesion_date')</th>
                                                <th>@lang('portfolio.sold')</th>
                                                <th>@lang('portfolio.edit')</th>
                                                <th>@lang('portfolio.delete')</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($lands as $land)
                                            <tr>
                                                <td>{{$land->location}}</td>
                                                <td>{{$land->area}}</td>
                                                <td>{{number_format($land->price,2,'.','')}}</td>  
                                                <td>{{$land->commission_rate}}</td> 
                                                <td>{{date("d.m.Y", strtotime($land->possesion_date))}}</td>
                                                <td>{{$land->sold ? __('portfolio.yes') : __('portfolio.no')}}</td>
                                                <td>
                                                    <a class="btn btn-primary" href="{{route("lands.edit",['id'=>$land->id]) }}" role="button">@lang('portfolio.edit')</a></div>
                                                </td>
                                                <td>
                                                    {{ Form::open(['action' => ['LandController@destroy', 'id'=>$land->id], 'method' => 'delete','onsubmit' => 'return ConfirmDelete()']) }}
                                                        {{ Form::submit(__('portfolio.delete'), ['class'=>'btn btn-danger btn-mini']) }}
                                                    {{ Form::close() }}
                                                </td>
                                            </tr>
                                            @endforeach 
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-10 col-md-offset-2">{{ $lands->links() }}</div>
                            </div>
                        @else
                            <p align="center" style="color:red">@lang('message.no_data')</p> 
                        @endif
                    </div>   
                </div>
        </div>  
    </div>
@endsection