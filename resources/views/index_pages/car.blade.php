@extends('layouts.app')
@section('content')
    <div class='container'>
        <div class='row'>
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-primary">
                        <div class="panel-heading">@lang('portfolio.cars')</div>
                        @if($cars->count() >0)
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>@lang('portfolio.model')</th>
                                                <th>@lang('portfolio.price')</th>
                                                <th>@lang('portfolio.possesion_date')</th>
                                                <th>@lang('portfolio.commission_rate')</th>
                                                <th>@lang('portfolio.sold')</th>
                                                <th>@lang('portfolio.edit')</th>
                                                <th>@lang('portfolio.delete')</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($cars as $car)
                                            <tr>
                                                <td>{{$car->model}}</td>
                                                <td>{{$car->price}}</td> 
                                                <td>{{date("d.m.Y", strtotime($car->possesion_date))}}</td>
                                                <td>{{$car->commission_rate}}</td>
                                                <td>{{$car->sold ? __('portfolio.sold') : __('portfolio.not_sold')}}</td>
                                                <td>
                                                    <a class="btn btn-primary" href="{{route("cars.edit",['id'=>$car->id]) }}" role="button">@lang('portfolio.edit')</a></div>
                                                </td>
                                                <td>
                                                    {{ Form::open(['action' => ['CarController@destroy', 'id'=>$car->id], 'method' => 'delete','onsubmit' => 'return ConfirmDelete()']) }}
                                                        {{ Form::submit(__('portfolio.delete'), ['class'=>'btn btn-danger btn-mini']) }}
                                                    {{ Form::close() }}
                                                </td>
                                            </tr>
                                            @endforeach 
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-10 col-md-offset-2">{{ $cars->links() }}</div>
                            </div>
                        @else
                            <p align="center" style="color:red">@lang('message.no_data')</p> 
                        @endif
                    </div>   
                </div>
        </div>  
    </div>
@endsection