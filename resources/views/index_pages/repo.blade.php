@extends('layouts.app')
@section('content')
    <div class='container'>
        <div class='row'>
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-primary">
                        <div class="panel-heading">@lang('portfolio.repos')</div>
                        @if($repos->count() >0)
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>@lang('portfolio.name_of_bank')</th>
                                                <th>@lang('portfolio.currency')</th>
                                                <th>@lang('portfolio.amount')</th>
                                                <th>@lang('portfolio.interest_rate')</th>
                                                <th>@lang('portfolio.withholding_rate')</th>
                                                <th>@lang('portfolio.date_start')</th>
                                                <th>@lang('portfolio.date_end')</th>
                                                <th>@lang('portfolio.edit')</th>
                                                <th>@lang('portfolio.delete')</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($repos as $repo)
                                            <tr>
                                                <td>{{$repo->name_of_bank}}</td>
                                                <td>{{$repo->currency}}</td>
                                                <td>{{number_format($repo->amount,2,'.','')}}</td>  
                                                <td>{{$repo->interest_rate}}</td>
                                                <td>{{$repo->withholding_rate}}</td>
                                                <td>{{date("d.m.Y", strtotime($repo->start))}}</td>
                                                <td>{{date("d.m.Y", strtotime($repo->end))}}</td>
                                                <td>
                                                    <a class="btn btn-primary" href="{{route("repos.edit",['id'=>$repo->id]) }}" role="button">@lang('portfolio.edit')</a></div>
                                                </td>
                                                <td>
                                                    {{ Form::open(['action' => ['RepoController@destroy', 'id'=>$repo->id], 'method' => 'delete','onsubmit' => 'return ConfirmDelete()']) }}
                                                        {{ Form::submit(__('portfolio.delete'), ['class'=>'btn btn-danger btn-mini']) }}
                                                    {{ Form::close() }}
                                                </td>
                                            </tr>
                                            @endforeach 
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-10 col-md-offset-2">{{ $repos->links() }}</div>
                            </div>
                        @else
                            <p align="center" style="color:red">@lang('message.no_data')</p> 
                        @endif
                    </div>   
                </div>
        </div>  
    </div>
@endsection