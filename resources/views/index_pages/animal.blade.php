@extends('layouts.app')
@section('content')
    <div class='container'>
        <div class='row'>
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-primary">
                        <div class="panel-heading">@lang('portfolio.animals')</div>
                        @if($animals->count() >0)
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>@lang('portfolio.type')</th>
                                                <th>@lang('portfolio.amount')</th>
                                                <th>@lang('portfolio.price')</th>
                                                <th>@lang('portfolio.commission_rate')</th>
                                                <th>@lang('portfolio.possesion_date')</th>
                                                <th>@lang('portfolio.sold')</th>
                                                <th>@lang('portfolio.edit')</th>
                                                <th>@lang('portfolio.delete')</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($animals as $animal)
                                            <tr>
                                                <td>{{$animal->type}}</td>
                                                <td>{{number_format($animal->amount,2,'.','')}}</td> 
                                                <td>{{$animal->price}}</td> 
                                                <td>{{$animal->commission_rate}}</td> 
                                                <td>{{date("d.m.Y", strtotime($animal->possesion_date))}}</td>
                                                <td>{{$animal->sold ? __('portfolio.yes') : __('portfolio.no')}}</td>
                                                <td>
                                                    <a class="btn btn-primary" href="{{route("animals.edit",['id'=>$animal->id] ) }}" role="button">@lang('portfolio.edit')</a></div>
                                                </td>
                                                <td>
                                                    {{ Form::open(['action' => ['AnimalController@destroy', 'id'=>$animal->id], 'method' => 'delete','onsubmit' => 'return ConfirmDelete()']) }}
                                                        {{ Form::submit(__('portfolio.delete'), ['class'=>'btn btn-danger btn-mini']) }}
                                                    {{ Form::close() }}
                                                </td>
                                            </tr>
                                            @endforeach 
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-10 col-md-offset-2">{{ $animals->links() }}</div>
                            </div>
                        @else
                            <p align="center" style="color:red">@lang('message.no_data')</p> 
                        @endif
                    </div>   
                </div>
        </div>  
    </div>
@endsection