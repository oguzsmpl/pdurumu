@extends('layouts.app')
@section('content')
    <div class='container'>
        <div class='row'>
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-primary">
                        <div class="panel-heading">@lang('portfolio.stocks')</div>
                        @if($stocks->count() >0)
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>@lang('portfolio.name')</th>
                                                <th>@lang('portfolio.amount')</th>
                                                <th>@lang('portfolio.price')</th>
                                                <th>@lang('portfolio.price_current')</th>
                                                <th>@lang('portfolio.possesion_date')</th>
                                                <th>@lang('portfolio.sold')</th>
                                                <th>@lang('portfolio.edit')</th>
                                                <th>@lang('portfolio.delete')</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($stocks as $stock)
                                            <tr>
                                                <td>{{$stock->name}}</td>
                                                <td>{{number_format($stock->amount,2,'.','')}}</td><td>{{number_format($stock->price,2,'.','')}}</td>
                                                <td>{{$stock->price_current}}</td> 
                                                <td>{{date("d.m.Y", strtotime($stock->possesion_date))}}</td>
                                                <td>{{$stock->sold ? __('portfolio.yes') : __('portfolio.no')}}</td>
                                                <td>
                                                    <a class="btn btn-primary" href="{{route("stocks.edit",['id'=>$stock->id]) }}" role="button">@lang('portfolio.edit')</a></div>
                                                </td>
                                                <td>
                                                    {{ Form::open(['action' => ['StockController@destroy', 'id'=>$stock->id], 'method' => 'delete','onsubmit' => 'return ConfirmDelete()']) }}
                                                        {{ Form::submit(__('portfolio.delete'), ['class'=>'btn btn-danger btn-mini']) }}
                                                    {{ Form::close() }}
                                                </td>
                                            </tr>
                                            @endforeach 
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-10 col-md-offset-2">{{ $stocks->links() }}</div>
                            </div>
                        @else
                            <p align="center" style="color:red">@lang('message.no_data')</p> 
                        @endif
                    </div>   
                </div>
        </div>  
    </div>
@endsection