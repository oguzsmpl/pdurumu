@extends('layouts.app')
@section('content')
    <div class='container'>
        <div class='row'>
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-primary">
                        <div class="panel-heading">@lang('portfolio.deposit_accounts')</div>
                        @if($deposits->count() >0)
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>@lang('portfolio.name_of_bank')</th>
                                                <th>@lang('portfolio.currency')</th>
                                                <th>@lang('portfolio.amount')</th>
                                                <th>@lang('portfolio.interest_rate')</th>
                                                <th>@lang('portfolio.withholding_rate')</th>
                                                <th>@lang('portfolio.date_start')</th>
                                                <th>@lang('portfolio.date_end')</th>
                                                <th>@lang('portfolio.edit')</th>
                                                <th>@lang('portfolio.delete')</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($deposits as $deposit)
                                            <tr>
                                                <td>{{$deposit->name_of_bank}}</td>
                                                <td>{{$deposit->currency}}</td>
                                                <td>{{number_format($deposit->amount,2,'.','')}}</td>  
                                                <td>{{$deposit->interest_rate}}</td>
                                                <td>{{$deposit->withholding_rate}}</td>
                                                <td>{{date("d.m.Y", strtotime($deposit->start))}}</td>
                                                <td>{{date("d.m.Y", strtotime($deposit->end))}}</td>
                                                <td>
                                                    <a class="btn btn-primary" href="{{route("deposits.edit",['id'=>$deposit->id]) }}" role="button">@lang('portfolio.edit')</a></div>
                                                </td>
                                                <td>
                                                    {{ Form::open(['action' => ['DepositController@destroy', 'id'=>$deposit->id], 'method' => 'delete','onsubmit' => 'return ConfirmDelete()']) }}
                                                        {{ Form::submit(__('portfolio.delete'), ['class'=>'btn btn-danger btn-mini']) }}
                                                    {{ Form::close() }}
                                                </td>
                                            </tr>
                                            @endforeach 
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-10 col-md-offset-2">{{ $deposits->links() }}</div>
                            </div>
                        @else
                            <p align="center" style="color:red">@lang('message.no_data')</p> 
                        @endif
                    </div>   
                </div>
        </div>  
    </div>
@endsection