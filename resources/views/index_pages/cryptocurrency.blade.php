@extends('layouts.app')
@section('content')
    <div class='container'>
        <div class='row'>
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-primary">
                        <div class="panel-heading">@lang('portfolio.cryptocurrencies')</div>
                        @if($cryptocurrencies->count() >0)
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>@lang('portfolio.currency')</th>
                                                <th>@lang('portfolio.amount')</th>
                                                <th>@lang('portfolio.possesion_date')</th>
                                                <th>@lang('portfolio.spent')</th>
                                                <th>@lang('portfolio.edit')</th>
                                                <th>@lang('portfolio.delete')</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($cryptocurrencies as $cryptocurrency)
                                            <tr>
                                                <td>{{$cryptocurrency->currency}}</td>
                                                <td>{{number_format($cryptocurrency->amount,2,'.','')}}</td>  
                                                <td>{{date("d.m.Y", strtotime($cryptocurrency->possesion_date))}}</td>
                                                <td>{{$cash->spent ? __('portfolio.yes') : __('portfolio.no')}}</td>
                                                <td>
                                                    <a class="btn btn-primary" href="{{route("cryptocurrencies.edit",['id'=>$cryptocurrency->id]) }}" role="button">@lang('portfolio.edit')</a></div>
                                                </td>
                                                <td>
                                                    {{ Form::open(['action' => ['CryptocurrencyController@destroy', 'id'=>$cryptocurrency->id], 'method' => 'delete','onsubmit' => 'return ConfirmDelete()']) }}
                                                        {{ Form::submit(__('portfolio.delete'), ['class'=>'btn btn-danger btn-mini']) }}
                                                    {{ Form::close() }}
                                                </td>
                                            </tr>
                                            @endforeach 
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-10 col-md-offset-2">{{ $cryptocurrencies->links() }}</div>
                            </div>
                        @else
                            <p align="center" style="color:red">@lang('message.no_data')</p> 
                        @endif
                    </div>   
                </div>
        </div>  
    </div>
@endsection