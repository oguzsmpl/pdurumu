@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
             <div class="panel panel-default">
                 <div class="panel-heading">@lang('portfolio.update_informations')</div>
                 <div class="panel-body">
                    {{ Form::model($drawing,['method'=>'PATCH','route' => ['drawings.update', 'drawing_id'=>$drawing->id] ])}}
                        <div class="form-group">
                            {{Form::label('name_of_bank', __('portfolio.name_of_bank'))}}
                            {{Form::text('name_of_bank', null,['class' => 'form-control','required'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('iban', __('portfolio.iban'))}}
                            {{Form::text('iban', null,['class' => 'form-control'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('currency', __('portfolio.currency'))}}
                            {{Form::select('currency',[''=>__('portfolio.select'),'try'=>'TRY','usd'=>'USD','eur'=>'EUR','gbp'=>'GBP','rub'=>'RUB','jpy'=>'JPY','cny'=>'CNY','inr'=>'INR','chf'=>'CHF','aud'=>'AUD','cad'=>'CAD'],null,['class' => 'form-control','required'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('amount', __('portfolio.amount'))}}
                            {{Form::number('amount',null,['class' => 'form-control','required','step'=>'any'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('start', __('portfolio.date_start'))}}
                            {{Form::text('start',date("d.m.Y", strtotime($drawing->start)),['class' => 'form-control', 'id'=>'datepicker'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('end', __('portfolio.date_end'))}}
                            {{Form::text('end',$drawing->end == null ? null : date("d.m.Y", strtotime($drawing->end)),['class' => 'form-control', 'id'=>'datetimepicker'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('using', __('portfolio.using'))}}
                            {{Form::select('using',[''=>__('portfolio.select'),true=>__("portfolio.yes"),false=>__('portfolio.no')],null,['class' => 'form-control'])}}
                        </div>
                        <div class="form-group">
                            {{Form::submit(__('portfolio.update'),['class' => 'btn-lg btn-success'])}}
                        </div>
                    {{ Form::close() }}
                 </div>
             </div>
        </div>
    </div>   
</div>
@endsection()