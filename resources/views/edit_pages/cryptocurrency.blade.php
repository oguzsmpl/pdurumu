@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
             <div class="panel panel-default">
                 <div class="panel-heading">@lang('portfolio.update_informations')</div>
                 <div class="panel-body">
                    {{ Form::model($cryptocurrency,['method'=>'PATCH','route' => ['cryptocurrencies.update', 'cryptocurrency_id'=>$cryptocurrency->id] ])}}
                        <div class="form-group">
                            {{Form::label('currency', __('portfolio.currency'))}}
                            {{Form::select('currency',[''=>__('portfolio.select'),'btc'=>'BTC','xrp'=>'XRP','eth'=>'ETH','bch'=>'BCH','ltc'=>'LTC'],null,['class' => 'form-control','required'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('amount', __('portfolio.amount'))}}
                            {{Form::number('amount',null,['class' => 'form-control','required','step'=>'any'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('possesion_date', __('portfolio.possesion_date'))}}
                            {{Form::text('possesion_date',date("d.m.Y", strtotime($cryptocurrency->possesion_date)),['class' => 'form-control', 'id'=>'datepicker'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('spent', __('portfolio.spent'))}}
                            {{Form::select('spent',[''=>__('portfolio.select'),true=>__("portfolio.yes"),false=>__('portfolio.no')],null,['class' => 'form-control','required','onchange' => 'return nullSpendingDateAndWhySpent("spent","spending_date","why_spent")','id'=>'spent'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('spending_date', __('portfolio.spending_date'))}}
                            {{Form::text('spending_date',isset($cryptocurrency->spending_date) ? date("d.m.Y", strtotime($cryptocurrency->spending_date)) : "",['class' => 'form-control', 'id'=>'spending_date'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('why_spent', __('portfolio.why_spent'))}}
                            {{Form::select('why_spent',[''=>__('portfolio.select'),'Entertainment'=>__('portfolio.entertainment'),'Education'=>__('portfolio.education'),'Shopping'=>__('portfolio.shopping'),'Health'=>__('portfolio.health'),'Kids'=>__('portfolio.kids'),'Food'=>__('portfolio.food'),'Gift'=>__('portfolio.gift'),'Investment'=>__('portfolio.investment'),'Bill'=>__('portfolio.bill'),'Transport'=>__('portfolio.transport'),'Tax'=>__('portfolio.tax'),'Other'=>__('portfolio.other')],null,['class' => 'form-control','id'=>'why_spent'])}}
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                {{Form::submit(__('portfolio.update'),['class' => 'btn-lg btn-success'])}}
                            </div>
                        </div>
                    {{ Form::close() }}
                 </div>
             </div>
        </div>
    </div>   
</div>
@endsection()