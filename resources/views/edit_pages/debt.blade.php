@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
             <div class="panel panel-default">
                 <div class="panel-heading">@lang('portfolio.update_informations')</div>
                 <div class="panel-body">
                    {{ Form::model($debt,['method'=>'PATCH','route' => ['debts.update', 'debt_id'=>$debt->id] ])}}
                        <div class="form-group">
                            {{Form::label('who', __('portfolio.who'))}}
                            {{Form::text('who', null,['class' => 'form-control','required'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('currency', __('portfolio.currency'))}}
                            {{Form::select('currency',[''=>__('portfolio.select'),'try'=>'TRY','usd'=>'USD','eur'=>'EUR','gbp'=>'GBP','rub'=>'RUB','jpy'=>'JPY','cny'=>'CNY','inr'=>'INR','chf'=>'CHF','aud'=>'AUD','cad'=>'CAD'],null,['class' => 'form-control','required'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('amount', __('portfolio.amount'))}}
                            {{Form::number('amount',null,['class' => 'form-control','required','step'=>'any'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('possesion_date', __('portfolio.possesion_date'))}}
                            {{Form::text('possesion_date',date("d.m.Y", strtotime($debt->possesion_date)),['class' => 'form-control', 'id'=>'datepicker'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('paid', __('portfolio.paid'))}}
                            {{Form::select('paid',[''=>__('portfolio.select'),true=>__("portfolio.yes"),false=>__('portfolio.no')],null,['class' => 'form-control','required','onchange' => 'return nullPaidDateAndWhyTaken()','id'=>'paid'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('paid_date', __('portfolio.paid_date'))}}
                            {{Form::text('paid_date',isset($debt->paid_date) ? date("d.m.Y", strtotime($debt->paid_date)) : null,['class' => 'form-control', 'id'=>'paid_date'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('why_taken', __('portfolio.why_taken'))}}
                            {{Form::select('why_taken',[''=>__('portfolio.select'),'Entertainment'=>__('portfolio.entertainment'),'Education'=>__('portfolio.education'),'Shopping'=>__('portfolio.shopping'),'Health'=>__('portfolio.health'),'Kids'=>__('portfolio.kids'),'Food'=>__('portfolio.food'),'Gift'=>__('portfolio.gift'),'Investment'=>__('portfolio.investment'),'Bill'=>__('portfolio.bill'),'Transport'=>__('portfolio.transport'),'Tax'=>__('portfolio.tax'),'Other'=>__('portfolio.other')],null,['class' => 'form-control','id'=>'why_taken'])}}
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                {{Form::submit(__('portfolio.update'),['class' => 'btn-lg btn-success'])}}
                            </div>
                        </div>
                    {{ Form::close() }}
                 </div>
             </div>
        </div>
    </div>   
</div>
@endsection()
@section('customJs')
<script type="text/javascript">
    function nullPaidDateAndWhyTaken(){
        if(document.getElementById("paid").value==0){
            document.getElementById("paid_date").value="";
        }
    }
</script>
@endsection()