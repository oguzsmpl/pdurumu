@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
             <div class="panel panel-default">
                 <div class="panel-heading">@lang('portfolio.update_informations')</div>
                 <div class="panel-body">
                    {{ Form::model($other,['method'=>'PATCH','route' => ['others.update', 'other_id'=>$other->id] ])}}
                        <div class="form-group">
                            {{Form::label('name_of_asset', __('portfolio.name_of_asset'))}}
                            {{Form::text('name_of_asset', null,['class' => 'form-control','required'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('currency', __('portfolio.currency'))}}
                            {{Form::select('currency',[''=>__('portfolio.select'),'try'=>'TRY','usd'=>'USD','eur'=>'EUR','gbp'=>'GBP','rub'=>'RUB','jpy'=>'JPY','cny'=>'CNY','inr'=>'INR','chf'=>'CHF','aud'=>'AUD','cad'=>'CAD'],null,['class' => 'form-control','required'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('amount', __('portfolio.amount'))}}
                            {{Form::number('amount',null,['class' => 'form-control','required','step'=>'any'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('price', __('portfolio.price'))}}
                            {{Form::number('price',null,['class' => 'form-control','required','step'=>'any'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('commission_rate', __('portfolio.commission_rate'))}}
                            {{Form::number('commission_rate',null,['class' => 'form-control','required','step'=>'any','type'=>'any'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('possesion_date', __('portfolio.possesion_date'))}}
                            {{Form::text('possesion_date',date("d.m.Y", strtotime($other->possesion_date)),['class' => 'form-control','id'=>'datetimepicker'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('sold', __('portfolio.sold'))}}
                            {{Form::select('sold',[''=>__('portfolio.select'),true=>__("portfolio.yes"),false=>__('portfolio.no')],null,['class' => 'form-control'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('price_sale', __('portfolio.price_sale'))}}
                            {{Form::number('price_sale',null,['class' => 'form-control','required','step'=>'any'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('sold_date', __('portfolio.sold_date'))}}
                            {{Form::text('sold_date',isset($other->sold_date) ? date("d.m.Y", strtotime($other->sold_date)) : "",['class' => 'form-control', 'id'=>'datepicker'])}}
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                {{Form::submit(__('portfolio.update'),['class' => 'btn-lg btn-success','required'])}}
                            </div>
                        </div>
                    {{ Form::close() }}
                 </div>
             </div>
        </div>
    </div>   
</div>
@endsection()