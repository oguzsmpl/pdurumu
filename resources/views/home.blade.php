@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header" style="text-align: center;">@lang('portfolio.overview_in_dollar')</div>
                <div class="card-body" id='pdurumu'>
                    @if($userHasNoAsset == true)
                    <p style="color:red">@lang('message.no_data')</p>
                    @else
                    {!! $lavaPdurumu->render('PieChart','Portfolio','pdurumu') !!}
                    @endif()
                </div>
            </div>
        </div>
        <div class="col-md-2" style="padding-top: 30px;">
            <a class="btn btn-info" href="{{route('wealth')}}" role="button">@lang('portfolio.view_wealth_change')
            </a>
        </div>

    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="exampleFormControlSelect1">@lang('portfolio.select')</label>
                <select class="form-control" id="portId" onchange="return addHrefs();">
                    <option value="cashes">@lang('portfolio.cash')</option>
                    <option value="debts">@lang('portfolio.debt')</option>
                    <option value="deposits">@lang('portfolio.deposit_account')</option>
                    <option value="drawings">@lang('portfolio.drawing_account')</option>
                    <option value="commodities">@lang('portfolio.commodity')</option>
                    <option value="bonds">@lang('portfolio.bond')</option>
                    <option value="repos">@lang('portfolio.repo')</option>
                    <option value="stocks">@lang('portfolio.stock')</option>
                    <option value="cryptocurrencies">@lang('portfolio.cryptocurrency')</option>
                    <option value="houses">@lang('portfolio.house')</option>
                    <option value="cars">@lang('portfolio.car')</option>
                    <option value="lands">@lang('portfolio.land')</option>
                    <option value="animals">@lang('portfolio.animal')</option>
                    <option value="others">@lang('portfolio.other')</option>                    
                </select>
            </div>
        </div>
        <div class="col-md-1" style="padding-top: 30px;">
            <a class="btn btn-primary" href="{{route('cashes.index')}}" role="button" id="indexPage">@lang('portfolio.view')</a>
        </div>
        <div class="col-md-1" style="padding-top: 30px;">
            <a class="btn btn-success" href="{{route('cashes.create')}}" role="button" id="createPage">@lang('portfolio.create')</a>
        </div>
        <div class="col-md-6"></div>
    </div>
</div>
@endsection

@section('customJs')
    <script type="application/javascript">
    function addHrefs(){
        var assetType = document.getElementById("portId").value;
        var hrefIndex = "{{url()->current()}}"; 
        document.getElementById('indexPage').href= hrefIndex+ "/" + assetType;
        document.getElementById('createPage').href= hrefIndex+ "/" + assetType+ "/create";
    }
    </script>      
@endsection