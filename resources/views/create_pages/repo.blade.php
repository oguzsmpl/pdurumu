@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('portfolio.repo')</div>
                    <div class="panel-body">
                        {{ Form::open(array('action' => ['RepoController@store'],'class' => 'form')) }}
                            <div class="form-group">
                                {{Form::label('name_of_bank', __('portfolio.name_of_bank'))}}
                                {{Form::text('name_of_bank', null,['class' => 'form-control','required'])}}
                            </div>
                            <div class="form-group">
                                {{Form::label('currency', __('portfolio.currency'))}}
                                {{Form::select('currency',[''=>__('portfolio.select'),'try'=>'TRY','usd'=>'USD','eur'=>'EUR','gbp'=>'GBP','rub'=>'RUB','jpy'=>'JPY','cny'=>'CNY','inr'=>'INR','chf'=>'CHF','aud'=>'AUD','cad'=>'CAD'],'',['class' => 'form-control','required'])}}
                            </div>
                            <div class="form-group">
                                {{Form::label('amount', __('portfolio.amount'))}}
                                {{Form::number('amount',null,['class' => 'form-control','required','step'=>'any'])}}
                            </div>
                            <div class="form-group">
                                {{Form::label('interest_rate', __('portfolio.interest_rate'))}}
                                {{Form::number('interest_rate',null,['class' => 'form-control','required','step'=>'any'])}}
                            </div>
                            <div class="form-group">
                                {{Form::label('withholding_rate', __('portfolio.withholding_rate'))}}
                                {{Form::number('withholding_rate',null,['class' => 'form-control','required','step'=>'any'])}}
                            </div>
                            <div class="form-group">
                                {{Form::label('start', __('portfolio.date_start'))}}
                                {{Form::text('start',date("d.m.Y"),['class' => 'form-control', 'id'=>'datepicker'])}}
                            </div>
                            <div class="form-group">
                                {{Form::label('end', __('portfolio.date_end'))}}
                                {{Form::text('end',date("d.m.Y",strtotime('+1 year')),['class' => 'form-control', 'id'=>'datetimepicker'])}}
                            </div>
                            <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                {{Form::submit(__('portfolio.create'),['class' => 'btn-lg btn-success','required'])}}
                            </div>
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection