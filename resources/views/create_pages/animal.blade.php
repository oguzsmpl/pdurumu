@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('portfolio.animal')</div>
                    <div class="panel-body">
                        {{ Form::open(array('action' => ['AnimalController@store'],'class' => 'form')) }}
                            <div class="form-group">
                                {{Form::label('type', __('portfolio.type'))}}
                                {{Form::text('type', null,['class' => 'form-control','required'])}}
                            </div>
                            <div class="form-group">
                                {{Form::label('amount', __('portfolio.amount'))}}
                                {{Form::number('amount',null,['class' => 'form-control','required','step'=>'any'])}}
                            </div>
                            <div class="form-group">
                                {{Form::label('currency', __('portfolio.currency'))}}
                                {{Form::select('currency',[''=>__('portfolio.select'),'try'=>'TRY','usd'=>'USD','eur'=>'EUR','gbp'=>'GBP','rub'=>'RUB','jpy'=>'JPY','cny'=>'CNY','inr'=>'INR','chf'=>'CHF','aud'=>'AUD','cad'=>'CAD'],'',['class' => 'form-control','required'])}}
                            </div>
                            <div class="form-group">
                                {{Form::label('price', __('portfolio.price'))}}
                                {{Form::number('price',null,['class' => 'form-control','required','step'=>'any'])}}
                            </div>
                            <div class="form-group">
                                {{Form::label('commission_rate', __('portfolio.commission_rate'))}}
                                {{Form::number('commission_rate',0,['class' => 'form-control','required','step'=>'any'])}}
                            </div>
                            <div class="form-group">
                                {{Form::label('possesion_date', __('portfolio.possesion_date'))}}
                                {{Form::text('possesion_date',date("d.m.Y"),['class' => 'form-control','id'=>'datetimepicker'])}}
                            </div>
                             <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    {{Form::submit(__('portfolio.create'),['class' => 'btn-lg btn-success','required'])}}
                                </div>
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection