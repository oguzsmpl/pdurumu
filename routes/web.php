<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'HomeController@index')->name('home');
Route::get('/locale/{locale}', function($locale){
    \Session::put('locale',$locale);
    return redirect()->back();
})->name('locale');
Route::resource('cashes', 'CashController');
Route::resource('debts', 'DebtController');
Route::resource('lands', 'LandController');
Route::resource('deposits', 'DepositController');
Route::resource('drawings', 'DrawingController');
Route::resource('commodities', 'CommodityController');
Route::resource('bonds', 'BondController');
Route::resource('repos', 'RepoController');
Route::resource('stocks', 'StockController');
Route::resource('cryptocurrencies', 'CryptocurrencyController');
Route::resource('houses', 'HouseController');
Route::resource('cars', 'CarController');
Route::resource('animals', 'AnimalController');
Route::resource('others', 'OtherController');
Route::get('/wealth','WealthController@index')->name('wealth');
